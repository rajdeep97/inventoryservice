package com.meru.InventoryService.messaging;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import com.meru.InventoryService.service.InventoryService;

@Component
public class QueueOperation {
	

	private final String INVENTORY_CREATE_QUEUE="inventory.CREATE";
	private final String INVENTORY_DELETE_QUEUE="inventory.DELETE";
	private final String ADD_INVENTORIES_TO_ORDER_REQUEST = "order.request.new.SET_INVENTORIES";
	private final String ORDER_REQUEST_READY = "order.request.new.READY";
	private final String ORDER_CONFIRMED = "order.CONFIRMED";

	@Autowired
	private InventoryService service;
	
	@JmsListener(destination = INVENTORY_CREATE_QUEUE)
	public void createEmptyInventoryForNewProduct(final Message msg)throws JMSException
	{
		if(msg instanceof MapMessage)
		{
			MapMessage mapmsg = (MapMessage)msg;
			if(mapmsg.propertyExists("productID"))
			{
				long productID=mapmsg.getLongProperty("productID");
				this.service.createEmptyInventoryForProductID(productID);
			}
		}
	}
	@JmsListener(destination = INVENTORY_DELETE_QUEUE)
	public void deleteInventoryForProductId(final Message msg)throws JMSException
	{
		if(msg instanceof MapMessage)
		{
			MapMessage mapmsg = (MapMessage)msg;
			if(mapmsg.propertyExists("productID"))
			{
				long productID=mapmsg.getLongProperty("productID");
				this.service.deleteInventoryForProductID(productID);
			}
		}
	}
	
	@JmsListener(destination = ADD_INVENTORIES_TO_ORDER_REQUEST)
	@SendTo(ORDER_REQUEST_READY)
	public String addInventoriesToOrderRequest(String jsonOrderRequest)
	{
		return this.service.addInventoriesToOrderRequest(jsonOrderRequest);
	}
	
	@JmsListener(destination = ORDER_CONFIRMED)
	public void updateInventoriesAfterSuccessfulOrder(String jsonOrder)
	{
		this.service.updateInventoriesAfterSuccessfulOrder(jsonOrder);
	}
}
