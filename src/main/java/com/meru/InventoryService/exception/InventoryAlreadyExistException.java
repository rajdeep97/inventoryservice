package com.meru.InventoryService.exception;

public class InventoryAlreadyExistException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3845037120087598230L;

	public InventoryAlreadyExistException(final String errorMessage)
	{
		super(errorMessage);
	}
}
