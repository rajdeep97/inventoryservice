package com.meru.InventoryService.exception;

public class InventoryNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InventoryNotFoundException(final String exceptionMessage)
	{
		super(exceptionMessage);
	}

}
