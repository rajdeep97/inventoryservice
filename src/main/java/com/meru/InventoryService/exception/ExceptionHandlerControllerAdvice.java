package com.meru.InventoryService.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerControllerAdvice extends ResponseEntityExceptionHandler{

	@ExceptionHandler(InventoryAlreadyExistException.class)
	public final ResponseEntity<ExceptionResponseEntity> handleInventoryAlreadyExistException(InventoryAlreadyExistException exception, WebRequest request)
	{
		ExceptionResponseEntity error = new ExceptionResponseEntity(new Date(), exception.getMessage(), request.getDescription(false));
		return new ResponseEntity<ExceptionResponseEntity>(error, HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(InventoryNotFoundException.class)
	public final ResponseEntity<ExceptionResponseEntity> handleInventoryNotFoundException(InventoryNotFoundException exception, WebRequest request)
	{
		ExceptionResponseEntity error = new ExceptionResponseEntity(new Date(), exception.getMessage(), request.getDescription(false));
		return new ResponseEntity<ExceptionResponseEntity>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(InvalidRequestException.class)
	public ResponseEntity<ExceptionResponseEntity> handleInvalidRequestException(InvalidRequestException exception, WebRequest request)
	{
		ExceptionResponseEntity error = new ExceptionResponseEntity(new Date(), exception.getMessage(), request.getDescription(false));
		return new ResponseEntity<ExceptionResponseEntity>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ExceptionResponseEntity> handleAllOtherException(Exception exception, WebRequest request)
	{
		ExceptionResponseEntity error = new ExceptionResponseEntity(new Date(), exception.getMessage(), request.getDescription(false));
		return new ResponseEntity<ExceptionResponseEntity>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
