package com.meru.InventoryService.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Inventory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long inventoryID;
	
	private long productID;
	
	private int productInventory;

	public long getInventoryID() {
		return inventoryID;
	}

	public void setInventoryID(long inventoryID) {
		this.inventoryID = inventoryID;
	}

	public long getProductID() {
		return productID;
	}

	public void setProductID(long productID) {
		this.productID = productID;
	}

	public int getProductInventory() {
		return productInventory;
	}

	public void setProductInventory(int productInventory) {
		this.productInventory = productInventory;
	}
	
	
}
