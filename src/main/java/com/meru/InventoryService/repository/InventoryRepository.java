package com.meru.InventoryService.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.meru.InventoryService.model.Inventory;

public interface InventoryRepository extends JpaRepository<Inventory, Long>{
	
	@Query("SELECT ivt FROM Inventory ivt WHERE ivt.productID=?1")
	public Optional<Inventory> getInventoryForProductID(long productID);

}
