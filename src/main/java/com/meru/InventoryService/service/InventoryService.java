package com.meru.InventoryService.service;

import com.meru.InventoryService.model.Inventory;

public interface InventoryService {

	public Inventory createEmptyInventoryForProductID(long productID);

	public void deleteInventoryForProductID(long productID);

	public Inventory getInventoryForProductId(long productId);

	public Inventory updateInventoryForProductId(long productId, Inventory updatedInventory);

	public void deleteInventoryForProductId(long productId);

	public String addInventoriesToOrderRequest(String jsonOrderRequest);

	public void updateInventoriesAfterSuccessfulOrder(String jsonOrder);

}
