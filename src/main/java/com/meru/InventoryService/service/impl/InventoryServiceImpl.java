package com.meru.InventoryService.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meru.InventoryService.exception.InventoryAlreadyExistException;
import com.meru.InventoryService.exception.InventoryNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meru.InventoryService.exception.InvalidRequestException;
import com.meru.InventoryService.model.Inventory;
import com.meru.InventoryService.external.model.OrderRequest;
import com.meru.InventoryService.repository.InventoryRepository;
import com.meru.InventoryService.service.InventoryService;
import com.meru.InventoryService.external.model.CartItem;
import com.meru.InventoryService.external.model.Order;
import com.meru.InventoryService.external.model.OrderItem;
import com.meru.InventoryService.external.model.Product;
@Service
public class InventoryServiceImpl implements InventoryService{

	@Autowired
	private InventoryRepository repository;
	
	@Override
	public Inventory createEmptyInventoryForProductID(long productID) {
		Optional<Inventory> existingInventory = this.repository.getInventoryForProductID(productID);
		if(existingInventory.isPresent())
		{
			throw new InventoryAlreadyExistException("Inventory already exist for ProductId : "+productID);
		}
		
		Inventory inventory = new Inventory();
		inventory.setProductID(productID);
		inventory.setProductInventory(50);
		return this.repository.save(inventory);
	}

	@Override
	public void deleteInventoryForProductID(long productID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Inventory getInventoryForProductId(long productId) {
		Optional<Inventory> inventory = this.repository.getInventoryForProductID(productId);
		if(inventory.isPresent())
		{
			//System.out.println("Cart present "+cart.get().getCustomerID());
			return inventory.get();
		}
		else
		{
			throw new InventoryNotFoundException("No Inventory found for ProductId : "+productId);
		}
	}

	@Override
	public Inventory updateInventoryForProductId(long productId, Inventory updatedInventory) {
		if(updatedInventory.getProductID() != productId)
		{
			throw new InvalidRequestException("ProductId mismatch in request URI and body");
		}
		Optional<Inventory> inventory = this.repository.getInventoryForProductID(productId);
		if(inventory.isPresent())
		{
			return this.repository.save(updatedInventory);
		}
		else
		{
			throw new InventoryNotFoundException("No Inventory found for ProductId : "+productId);
		}
	}

	@Override
	public void deleteInventoryForProductId(long productId) {
		Optional<Inventory> inventory = this.repository.getInventoryForProductID(productId);
		if(inventory.isPresent())
		{
			this.repository.deleteById(productId);
		}
		else
		{
			throw new InventoryNotFoundException("No Inventory found for ProductId : "+productId);
		}
		
	}

	@Override
	public String addInventoriesToOrderRequest(String jsonOrderRequest) {
		ObjectMapper om = new ObjectMapper();
		String inventoriesAddedOrder = null;
		try {
			OrderRequest orderRequest = om.readValue(jsonOrderRequest, OrderRequest.class);
			List<CartItem> shoppingCart = orderRequest.getCart().getShoppingCart(); 
			if(shoppingCart.size()==0)
			{
				return jsonOrderRequest;
			}
			List<Inventory> inventories = new ArrayList<Inventory>();
			for(CartItem cartItem : shoppingCart)
			{
				Inventory inventory = this.getInventoryForProductId(cartItem.getProductID());
				inventories.add(inventory);
			}
			
			orderRequest.setInventories(inventories);
			inventoriesAddedOrder = om.writeValueAsString(orderRequest);
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return inventoriesAddedOrder;
	}

	@Override
	public void updateInventoriesAfterSuccessfulOrder(String jsonOrder) {
		ObjectMapper om = new ObjectMapper();
			try {
			Order order = om.readValue(jsonOrder, Order.class);
			List<OrderItem> orderItems = order.getOrderItems();
			Inventory inventory = null;
			for(OrderItem item : orderItems)
			{
				long productID = item.getProductID();
				inventory = this.getInventoryForProductId(productID);
				int quantityInStock = inventory.getProductInventory();
				int quantityBought = item.getQuantity();
				int updatedStock = quantityInStock - quantityBought;
				
				inventory.setProductInventory(updatedStock);
				this.updateInventoryForProductId(productID, inventory);
			}
			
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
	}

}
