package com.meru.InventoryService.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meru.InventoryService.model.Inventory;
import com.meru.InventoryService.service.InventoryService;

@RestController
public class InventoryController {

	@Autowired
	private InventoryService service;
	
	@PostMapping("/inventory/{productId}")
	public ResponseEntity<Inventory> createEmptyInventory(@PathVariable long productId)
	{
		Inventory newInventory = this.service.createEmptyInventoryForProductID(productId);
		return new ResponseEntity<Inventory>(newInventory,HttpStatus.CREATED);
	}
	
	@GetMapping("/inventory/{productId}")
	public Inventory getInventoryOfProduct(@PathVariable long productId)
	{
		return this.service.getInventoryForProductId(productId);	
	}
	
	@PutMapping("/inventory/{productId}")
	public Inventory updateInventoryOfProduct(@PathVariable long productId, @RequestBody Inventory updatedInventory)
	{
		return this.service.updateInventoryForProductId(productId,updatedInventory);
	}
	
	@DeleteMapping("/inventory/{productId}")
	public void deleteInventoryOfProduct(@PathVariable long productId)
	{
		this.service.deleteInventoryForProductId(productId);
	}
	
}
